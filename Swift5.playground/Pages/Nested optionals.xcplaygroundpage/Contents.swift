import Foundation

// Flattening nested optionals resulting from try

// Modifies the way try? works so that nested optionals are flattened to become regular optionals. This work the same as optionalss chaining and conditional typecasts.


struct User {
    var id: Int
    
    init?(id: Int) {
        if id < 1 {
            return nil
        }
        
        self.id = id
    }
    
    func getMessages() throws -> String {
        return "No message"
    }
}

let user = User(id: 0)
let message = try? user?.getMessages()

// Beacause user is optional it uses optional chaining, and beacuse getMessages() cand throw it uses try? to convert the throwing method intro an optional, so we aend up whit nested optional.
// I swift 4.2 and erlier this would make messages a String??, but in Swift5.0 and later try? won't wrap tvalues in an optionals if they are allredy optional, so messages will just be a String?
