// Introduction

// Swift 5.0 is available in Xcode 10.2 and this relese bring ABI stability
// Swift 5.0 is source compatible whith Swift 4.2 only, future releses will be binary compatible whit Swift 5.0 thanks to ABI stability that will allow Swift standard library and runtime get embedded in the OS
//      - applications won't be needed to be distributed whit Swift runtime libraries.
//      - runtime can be more deeply integrated and optimized whith these host operating systems
//      - apple will be able to deliver platform frameworks using Swift in future OSes.
//      - third parties will also be able to ship binary frameworks written in Swift


//ABI stands for Application Binary Interface. At runtime, Swift program binaries interact with other libraries through an ABI. It defines many low level details for binary entities like how to call functions, how their data is represented in memory, where metadata is and how to access it.
// ABI stability means locking down the ABI to the point that future compiler versions can produce binaries conforming to the stable ABI.
// It enables binary compatibility between applications and libraries compiled with different Swift versions.

