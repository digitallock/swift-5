import Foundation

// Customizing string interpolation
// Swift's string interpolation system is more effiecent and more flexible then ever before

struct User {
    var name: String
    var age: Int
}

// We cann add a special string interpolation for the above struct

extension String.StringInterpolation {
    mutating func appendInterpolation(_ value: User) {
        appendInterpolation("My name is \(value.name) and i'm \(value.age)")
    }
}

// Now we can print this

let user = User(name: "Hanc Naum", age: 24)
print("User details: \(user)")

// This is no different from CustomStringConvertible protocol, so let's  try something more advance
// StringInterpolation can take as many parameters as you need

extension String.StringInterpolation {
    mutating func appendInterpolation(_ number: Int, style: NumberFormatter.Style) {
        let formatter = NumberFormatter()
        formatter.numberStyle = style
        
        if let result = formatter.string(from: number as NSNumber) {
            appendLiteral(result)
        }
    }
}

// So NumberFormatter class has a number Style, including currency, ordinal & spell out. Like this we can spell out random numbers

let number = Int.random(in: 0...100)
print("The lucky number this week is \(number, style: .spellOut)")

// You can call appendLiteral as many times you want

extension String.StringInterpolation {
    mutating func appendInterpolation(repeat string: String, _ count: Int) {
        for _ in 0 ..< count {
            appendLiteral(string)
        }
    }
}

print("Good dog \(repeat: "wof ", 6)")

// As these are just regular expresion you can use Swift's full range o functionlity

extension String.StringInterpolation {
    mutating func appendInterpolation(_ values: [String], empty defaultValue: @autoclosure () -> String) {
        if values.count == 0 {
            appendLiteral(defaultValue())
        } else {
            appendLiteral(values.joined(separator: ", "))
        }
    }
}

let names = [String]()
print("List of students: \(names, empty: "No one").")

// using @autoclosure we can use simple values or call complex functions for the default Value

// We can use ExpressibleByStrungLiteral & ExpressibleByStringInterpolation protocols and add CustomStringConvertible to se what we can do

// Before we can make it work, we need to fulfill some criteria
// Any type we create should conform to theses protocols
// Inside your type needs to be a nested struct called StringInterpolation conforms to the StringInterpolationProtocol
// The nested struct needs to have an initializer that accepts two integers telling us roughly how much data it can expect.
// It also need to implement an appendLiteral() method, as well as one or more apppendIterpolation
// Your main types needs to have two initializers that allow it to be created from string literals and string interpolations

// We can put all that together into an example type that construct HTML from various common elements

struct ComplexText: ExpressibleByStringLiteral, ExpressibleByStringInterpolation, CustomStringConvertible {
    struct StringInterpolation: StringInterpolationProtocol {
        var output = ""
        
        //alocate enough space
        init(literalCapacity: Int, interpolationCount: Int) {
            output.reserveCapacity(literalCapacity * 2)
        }
        
        // append
        mutating func appendLiteral(_ literal: String) {
            print("Appending \(literal)")
            output.append(literal)
        }
        
        // user description
        mutating func appendInterpolation(description: String) {
            print("Appending \(description)")
            output.append("About me:\(description)")
        }
        
        // user contact info
        mutating func appendInterpolation(email: String, skypeID: String) {
            print("Appending \(email)")
            output.append("My contacts details are:\n  \(email)\n  skypeID: \(skypeID)")
        }
    }

    // the finished text for this whole component
    let description: String
    
    // create an instance from a literal string
    init(stringLiteral value: String) {
        description = value
    }
    
    // create an instance from an interpolated string
    init(stringInterpolation: StringInterpolation) {
        description = stringInterpolation.output
    }
}
/*:
 We can now create and use an instance of `HTMLComponent` using string interpolation like this:
 */
let text: ComplexText = " \(description: "iOS Developer at 3PillarGlobal"),\n \(email: "naum.hanc@3pillarglobal.com", skypeID: "hancnaum (Romania)")"
print(text)
