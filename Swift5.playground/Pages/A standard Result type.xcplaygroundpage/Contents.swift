import Foundation

// A standard Result type

// Introduction of a Result type into the standard library give a simpler and cleaner way to handle complex code.
// Swift's Result type is an enum that has tow case: success & failure. They can have associated value but failure must be something that conforms to Error type.

enum SwiftError: Error {
    case invalidUrl
}

func firstFetchUnreadCount(from urlString: String, complition: @escaping (Result<Int, SwiftError>) -> Void) {
    guard let url = URL(string: urlString) else {
        complition(.failure(.invalidUrl))
        return
    }
    
    print("Fetching \(url.absoluteString)...")
    complition(.success(5))
}

// To use that code we need to check the value from inside our Result to see if our call is succeeded or failed.

firstFetchUnreadCount(from: "https://www.hackingwithswift.com") { result in
    switch result {
    case .success(let count):
        print("\(count) unread messages.")
    case .failure(let error):
        print(error)
    }
}


// Result has a get() method that either returns the successful or throws its error otherwise, this can be convert into a regular throwing call

firstFetchUnreadCount(from: "https://www.hackingwithswift.com") { result in
    if let count = try? result.get() {
        print("\(count) unread messages.")
    }
}

// Result accepts throwing closure, if returns a successfully that gets used for the success case, otherwise its goes in the failure case

let url = URL(string: "https://www.hackingwithswift.com")
let result = Result { try String(contentsOf: url!)}


// You can also use the general Error protocol insted of the enum that you've created
// (Result<Int, Swift5Error>) ->  (Result<Int, Error>)

// Transforming Result
// Result has four methods that may be useful somehow: map(), flatMap(), mapError() & flatMapError()
// map() method -> thransforms the success value into a different kind of value using a closure you specify if it finds failure will return the directly and ignores your transformation


enum FactorError: Error {
    case belowMinimum
    case isPrime
}

func generateRandomNumber(maximum: Int) -> Result<Int, FactorError> {
    guard maximum >= 0 else { return .failure(.belowMinimum) }
    let number = Int.random(in: 0...maximum)
    return .success(number)
}

let resultNumber = generateRandomNumber(maximum:11)
let stringResultNumber = resultNumber.map { _ in "The random number is: \(resultNumber)." }
print(resultNumber)

// if we pass a positive number, result will be success so our mapp will tahe that number and use it with our string interpolation, then return another Result type, this time will be of the type Result<Int, FactorError>
// if the number is below 0, result would be set on failure case with FactorError.belowMinimum.
// using map would still return a Result<String, FactorError>

// Now we can transform our success type to another type

func calculateFactors(for number: Int) -> Result<Int, FactorError> {
    let factors = (1...number).filter { number % $0 == 0 }
    guard factors.count != 2 else { return .failure(.isPrime) }
    return .success(factors.count)
}

// Now we can use map to transform the output of generateRandomNumber() using calculateFactors()

let calculateFactorResult = resultNumber.map { calculateFactors(for: $0) }
print(calculateFactorResult)
// this make calculateFactorResult a bit ugly type: Result<Result<Int, FactorError>, FactorError>. you get a result with an result in it.
// so to avoid this we can just use flatMap

let calculateFactorResultFlat = resultNumber.flatMap { calculateFactors(for: $0) }
print(calculateFactorResultFlat)

