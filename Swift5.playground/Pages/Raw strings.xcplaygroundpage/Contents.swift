import Foundation

    // " -> #"
    let rain = #"The "rain" in "Spain" falls mainly on the Spaniards."#
    
    // treats the backslash as being a literal character in the string
    let keypaths = #"Swift keypaths such as \Person.name hold uninvoked references to properties."#
    
    // you need to add an extra # if you want string interpolation
    let answer = 42
    let dontpanic = #"The answer to life, the universe, and everything is \#(answer)."#
    
    // in this situation we can change our delimiter from #" to ##"
    let str = ##"My dog said "woof"#gooddog"##
    
    // you can use #""" & """# for multiline
    let multiline = #"""
    The answer to life,
    the universe,
    and everything is \#(answer).
    """#
    
    // more clean for reguler expressions
    
    let exp1 = "\\\\[A-Z]+[A-Za-z]+\\.[a-z]+"
    let exp2 = #"\\[A-Z]+[A-Za-z]+\.[a-z]+"#
  
    
    

