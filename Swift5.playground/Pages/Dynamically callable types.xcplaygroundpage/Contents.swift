import Foundation

// Dynamically callable types

// We got a new attribut to Swift, called @dynamicCallable, wich can make a type being directly callable.
// @dynamicCallable is the natural extension fo Swift 4.2 @dynamicMamberLookup, and serves the same purpose to make it easier for swift code to work along side dynamic languages

@dynamicCallable
struct RandomNumberGenerator1 {
    func dynamicallyCall(withKeywordArguments args: KeyValuePairs<String, Int>) -> Double {
        let numberOfZeroes = Double(args.first?.value ?? 0)
        let maximum = pow(10, numberOfZeroes)
        return Double.random(in: 0...maximum)
    }
}

// The method can be called with any number of parameters

let random1 = RandomNumberGenerator1()
let result1 = random1(numberOfZeroes: 0)

// If you had used dynamicallyCall(withArguments:) instead – or at the same time, because you can have them both a single type – then you’d write this:

@dynamicCallable
struct RandomNumberGenerator2 {
    func dynamicallyCall(withArguments args: [Int]) -> Double {
        let numberOfZeroes = Double(args[0])
        let maximum = pow(10, numberOfZeroes)
        return Double.random(in: 0...maximum)
    }
}

let random2 = RandomNumberGenerator2()
let result2 = random2(0)

// Rules to be aware of when using @dynamicCallable

// You can apply it to structs, enums, classes and protocols.
// If you implement withKeywordsArguents: or withArguments:, your type can still be called without parameter labels, just write empty string for key.
// If your implementations of withKeywordArguments: or withArguments: are marked as throwing, calling the type will also be throwing.
// You can't add @dynamicCallable to an extension
// You can still and methods and properties to your type as usually
