import Foundation

// Adds an isMultiple(of:) method to integers

let number = 4

print(number.isMultiple(of: 2))

