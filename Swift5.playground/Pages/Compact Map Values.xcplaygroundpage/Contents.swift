import Foundation

// Adds a new compactMapValue() method to dictionaries, bringing together the compactMap() with mapValues()


let times = [
    "Hudson": "38",
    "Clarke": "42",
    "Robinson": "35",
    "Hartis": "DNF"
]

// You can create an new dictionary with names and times as integaer only
let finishers = times.compactMapValues { Int($0) }
print(finishers)

// You can also use compactMapValues() to unwrap optionals and disacrd nil values

let people = [
    "Paul": 38,
    "Sophie": 8,
    "Charlotte": 5,
    "William": nil
]

let knownAges = people.compactMapValues { $0 }
print(knownAges)
